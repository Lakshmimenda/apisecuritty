package com.greatlearning.user.controller;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/User")
public class UserController {
// User Controller allowing the user to chat with admin and other users
	
	//implements base64 to per	form encoding & decoding  for secure message transfer
	
	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;
	// temp lists to store the data communicated.
	List<String> adminMessages = new ArrayList<String>();
	
	List<String> userMessages = new ArrayList<String>();

	// Kafka Listener consuming Admin Messages
	@KafkaListener(topics = { "admin" })
	public void getAdminMessage(String message)  {
		// base64 decoding the encoded message sent
		byte[] decodedBytes = Base64.getDecoder().decode(message);
		System.out.println("Kafka event consumed is: " + new String(decodedBytes));
		adminMessages.add(new String(decodedBytes));
	}
 
	// Kafka Listener consuming Other User Messages
	@KafkaListener(topics = { "user" })
	public void getUserMessages(String message) {
		// base64 decoding the encoded message sent
		byte[] decodedBytes = Base64.getDecoder().decode(message);
		System.out.println("Kafka event consumed is: " + new String(decodedBytes));
		userMessages.add(new String(decodedBytes));
	}

	// Allows user to send messages to the Admin
	@PostMapping("/produceAdminMessage")
	public String sendAdminMessage(@RequestBody String message) throws UnsupportedEncodingException {
		// base64 encoding
		byte[] encodedBytes = Base64.getEncoder().encode(message.getBytes("UTF-8"));
	 kafkaTemplate.send("admin",new String(encodedBytes));
		return "Message Sent";
	}
	
	// Allows the user to send messages to other Users
	@PostMapping("/produceUserMessage")
	public String sendUserMessage(@RequestBody String message) throws UnsupportedEncodingException {
		// base64 encoding
		byte[] encodedBytes = Base64.getEncoder().encode(message.getBytes("UTF-8"));
	 kafkaTemplate.send("user",new String(encodedBytes));
		return "Message Sent";
	}
	
	// Displays chat between the user and admin
	@GetMapping("/recieveAdminMessages")
	public List<String> getAdminMessages(){
		return adminMessages;
	}
	
	// Displays the chat between the user and other user
	@GetMapping("/recieveUserMessages")
	public List<String> getUserMessages(){
		return userMessages;
	}
}
