package com.greatlearning.user.controller;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/OfflineUser")
public class OfflineUserController {
// Offline User Controller allowing the user to communicate with Admin & Other Users
	
	// Implements base64 to encode and decode to provide secure message transfer
	
	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;

	// Temp List to store the Messages Communicated between User/Admin
	List<String> adminMessages = new ArrayList<String>();

	List<String> userMessages = new ArrayList<String>();

	// Kafka Listener to get the messages sent by admin
	@KafkaListener(topics = { "adminOffline" })
	public void getAdminMessage(String message) {
		// base64 decoding the encoded message sent
		byte[] decodedBytes = Base64.getDecoder().decode(message);
		System.out.println("Kafka event consumed is: " + new String(decodedBytes));
		adminMessages.add(new String(decodedBytes));
	}

	// Kafka Listener to consume the messages sent by other users
	@KafkaListener(topics = { "userOffline" })
	public void getUserMessages(String message) {
		// base64 decoding the encoded message sent
		byte[] decodedBytes = Base64.getDecoder().decode(message);
		System.out.println("Kafka event consumed is: " +  new String(decodedBytes));
		userMessages.add( new String(decodedBytes));
	}

	// Allows the user to send messages to Admin
	@PostMapping("/produceAdminMessage")
	public String sendAdminMessage(@RequestBody String message) throws UnsupportedEncodingException {
		// base64 encoding
		byte[] encodedBytes = Base64.getEncoder().encode(message.getBytes("UTF-8"));
		kafkaTemplate.send("adminOffline", new String(encodedBytes));
		return "Message Sent";
	}

	// Allows the user to send messages to other users
	@PostMapping("/produceUserMessage")
	public String sendUserMessage(@RequestBody String message) throws UnsupportedEncodingException {
		// base64 encoding
		byte[] encodedBytes = Base64.getEncoder().encode(message.getBytes("UTF-8"));
		kafkaTemplate.send("userOffline", new String(encodedBytes));
		return "Message Sent";
	}

	// displays the chat between the user and admin
	@GetMapping("/recieveAdminMessages")
	public List<String> getAdminMessages() {
		return adminMessages;
	}

   // displays the chat between the user and other user
	@GetMapping("/recieveUserMessages")
	public List<String> getUserMessages() {
		return userMessages;
	}

}
