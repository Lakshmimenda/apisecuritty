package com.greatlearning.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaAdminMicroservice {

// Main method	
	public static void main(String[] args) {
		SpringApplication.run(KafkaAdminMicroservice.class, args);
	}

}
