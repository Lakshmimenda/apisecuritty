package com.greatlearning.admin.controller;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AdminController {

	// Admin Controller allowing the admin to send and recieve messages from users.
	// implments base64 encoding and decoding to provide secure message transfer

	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;

	// Defining temp List to display the data been interchanged between user and
	// admin.
	List<String> messages = new ArrayList<String>();
	List<String> messagesOffline = new ArrayList<String>();

	// Kafka Admin Listener
	@KafkaListener(topics = { "admin" })
	public void getTopics(String message) {

		// base64 decoding the encoded message sent
		byte[] decodedBytes = Base64.getDecoder().decode(message);
		System.out.println("Kafka event consumed is: " + new String(decodedBytes));
		messages.add(new String(decodedBytes));
	}

	// Allows the admin to send message to the user
	@PostMapping("/produce")
	public String sendMessage(@RequestBody String message) throws UnsupportedEncodingException {
		// base64 encoding
		byte[] encodedBytes = Base64.getEncoder().encode(message.getBytes("UTF-8"));
		kafkaTemplate.send("admin", new String(encodedBytes));
		return "Message Sent";
	}

	// displays the chat between user and admin
	@GetMapping("/recieve")
	public List<String> getMessages() {
		return messages;
	}

	// Kafka Offline User listerner consuming messages from adminOffline topic
	@KafkaListener(topics = { "adminOffline" })
	public void getTopicsOffline(String message) {
		// base64 decoding the encoded message sent
		byte[] decodedBytes = Base64.getDecoder().decode(message);
		System.out.println("Kafka event consumed is: " + new String(decodedBytes));
		messagesOffline.add(new String(decodedBytes));
	}

	// Allows the admin to send messages to offline users
	@PostMapping("/produceOfflineMessages")
	public String sendMessageOffline(@RequestBody String message) throws UnsupportedEncodingException {
		// base64 encoding
		byte[] encodedBytes = Base64.getEncoder().encode(message.getBytes("UTF-8"));
		kafkaTemplate.send("adminOffline", new String(encodedBytes));
		return "Message Sent";
	}

	// displays the chat between admin and offline user
	@GetMapping("/recieveOfflineMessages")
	public List<String> getMessagesOffline() {
		return messagesOffline;
	}
}
